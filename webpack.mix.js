const mix = require('laravel-mix');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const imageminMozjpeg = require('imagemin-mozjpeg');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/js/app.ts', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .webpackConfig({
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    exclude: /node_modules/,
                },
            ],
        },
        resolve: {
            extensions: [
                '*',
                '.js',
                '.jsx',
                '.vue',
                '.ts',
                '.tsx',
                '.scss',
                '.jpg',
                '.jpeg',
                '.png',
                '.gif',
                '.svg',
            ],
        },
        plugins: [
            new CopyWebpackPlugin([
                {
                    from: 'resources/images',
                    to: 'img', // Laravel mix will place this in 'public/img'
                },
            ]),
            new ImageminPlugin({
                test: /\.(jpe?g|png|gif|svg)$/i,
                plugins: [
                    imageminMozjpeg({
                        quality: 80,
                    }),
                ],
            }),
        ],
    })
    .browserSync('localhost:8080');
