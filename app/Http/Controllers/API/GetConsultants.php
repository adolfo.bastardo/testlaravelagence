<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetConsultants extends Controller
{
    /**
     * Display a list of cao_usuario's related to permissao_sistema table.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::select(
            'select `cao_usuario`.*, `permissao_sistema`.*
        FROM `cao_usuario`
            INNER JOIN `permissao_sistema` ON `permissao_sistema`.`co_usuario` = `cao_usuario`.`co_usuario`
            WHERE `permissao_sistema`.`co_sistema` = ? AND `permissao_sistema`.`in_ativo` = ? AND (`permissao_sistema`.`co_tipo_usuario` = ? OR `permissao_sistema`.`co_tipo_usuario` = ? OR `permissao_sistema`.`co_tipo_usuario` = ?)',
            [1, 'S', 0, 1, 2]
        );

        return $users;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
