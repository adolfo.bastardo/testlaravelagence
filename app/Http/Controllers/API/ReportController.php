<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    /**
     * Filter cao_usuario table for get all reports of incomes and profits.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->input('type');
        $arrayPersonsFilter = explode(',', $request->input('arrayPersonsFilter'));
        $monthStart = $request->input('monthStart');
        $yearStart = $request->input('yearStart');
        $monthEnd = $request->input('monthEnd');
        $yearEnd = $request->input('yearEnd');

        $reports = [];
        $salary = 0;
        if ($type == 'consultants') {
            foreach ($arrayPersonsFilter as $consultant) {
                $ordersService = DB::select(
                    'select co_os
                    FROM `cao_os`
                    WHERE co_usuario = ?',
                    [$consultant]
                );

                $salaryR = DB::select(
                    'select brut_salario
                    FROM `cao_salario`
                    WHERE co_usuario = ?
                    LIMIT 1',
                    [$consultant]
                );

                foreach ($salaryR as $sr) {
                    $salary = $sr->brut_salario;
                }

                $bills = [];
                $totalNetIncome = 0;
                $totalCommission = 0;
                $totalProfit = 0;
                $totalSalary = 0;

                $sumCos = [];

                foreach ($ordersService as $os) {
                    $startTime = \DateTime::createFromFormat('m-Y', $monthStart . '-' . $yearStart);
                    $endTime = \DateTime::createFromFormat('m-Y', $monthEnd . '-' . $yearEnd);
                    $endTime->modify('+1 month');
                    $interval = new \DateInterval('P1M');

                    $daterange = new \DatePeriod($startTime, $interval, $endTime);
                    \setlocale(\LC_ALL, 'es_VE.utf8');

                    foreach ($daterange as $date) {
                        $month = $date->format("m");
                        $year = $date->format("Y");

                        $netIncome = 0;
                        $commission = 0;

                        $bill = DB::select(
                            'select *
                            FROM `cao_fatura`
                            WHERE co_os = ? AND YEAR(`data_emissao`) = ? AND MONTH(`data_emissao`) = ?',
                            [$os->co_os, $year, $month]
                        );

                        if (count($bill) > 0) {
                            foreach ($bill as $b1) {
                                $netIncome += $b1->valor - ($b1->valor * ($b1->total_imp_inc / 100));
                                $commission += ($b1->valor - ($b1->valor * ($b1->total_imp_inc / 100))) * ($b1->comissao_cn / 100);
                            }
                        }

                        $timeDate = \strftime('%B de %Y', $date->getTimestamp());

                        if (array_key_exists($timeDate, $sumCos)) {
                            $sumCos[$timeDate]["net_income"] += $netIncome;
                            $sumCos[$timeDate]["commission"] += $commission;
                        } else {
                            $sumCos[$timeDate] = [
                                "net_income" => $netIncome,
                                "commission" => $commission
                            ];
                        }
                    }
                }

                foreach ($sumCos as $key => $cos) {
                    $tempNet = $cos["net_income"];
                    $tempComm = $cos["commission"];
                    $tempProfit = $sumCos[$key]["profit"] = $tempNet - $salary - $tempComm;

                    $totalNetIncome += $tempNet;
                    $totalCommission += $tempComm;
                    $totalProfit += $tempProfit;
                    $totalSalary += $salary;
                }

                array_push($reports, [
                    "consultant" => $consultant,
                    "salary" => $salary,
                    "bills" => $sumCos,
                    "total" => [
                        "net_income" => $totalNetIncome,
                        "total_salary" => $totalSalary,
                        "commission" => $totalCommission,
                        "profit" => $totalProfit,
                    ]
                ]);
            }
        }

        return $reports;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
