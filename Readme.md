# TestLaravel for Agence's tests

#### Developed with Laravel, React, Material-UI and Docker

### Steps to reproduce this test

1. Install `docker` and `docker-compose`.
2. Install Composer and run `composer install` in terminal.
3. Install Node.js and Yarn (optionally), then, run `yarn` (or `npm install` if Yarn isn't installed) in terminal.
4. Run `$ docker-compose up -d`.
5. Open `localhost:8084` in your browser and login with mysql credentials (view in `docker-compose.yml` for it, the host is `mysql`).
6. Import `base_de_dados.sql` file in **test** database.
7. Run `cp .env.example .env` in terminal to set basic config for Laravel
8. At last, run `yarn prod` in terminal and open `localhost:8080` in your browser for view the test.

For view the in live test visit the [Heroku website for the test](http://aqueous-hamlet-81874.herokuapp.com/)
