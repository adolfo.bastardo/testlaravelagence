import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import NavbarComponent from './Navbar/Navbar';
import CommercialPerformanceComponent from './CommercialPerformance/CommercialPerformance';

export default class App extends Component {
    /**
     * Render JSX for all components
     */
    render() {
        return (
            <div>
                <NavbarComponent />
                <CommercialPerformanceComponent />
            </div>
        );
    }
}

/**
 * Append App to div if exist
 */
if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
