export interface Bill {
    date?: string;
    net_income: number;
    commission: number;
    profit: number;
}

export interface TotalBill extends Bill {
    total_salary: number;
}

export interface Report {
    consultant: string;
    salary: number;
    bills: Bill[];
    total: TotalBill;
}
