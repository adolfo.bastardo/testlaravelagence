import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Toolbar from '@material-ui/core/Toolbar';

import { Report } from './models';
import { Typography } from '@material-ui/core';

/**
 * General Report Table Props
 */
interface ReportProps {
    report: Report;
}

/**
 * General Report Table Layout
 */
export default class ReportComponent extends Component<ReportProps, any> {
    render() {
        const { report } = this.props;
        return (
            <Paper
                style={{ width: '100%', margin: '24px 0', overflowX: 'auto' }}
            >
                <Toolbar>
                    <Typography variant='h6'>{report.consultant}</Typography>
                </Toolbar>
                <Table style={{ width: '100%' }}>
                    <TableHead>
                        <TableRow>
                            <TableCell align='right'>Periodo</TableCell>
                            <TableCell align='right'>Ganancia Neta</TableCell>
                            <TableCell align='right'>Gasto Fijo</TableCell>
                            <TableCell align='right'>Comisión</TableCell>
                            <TableCell align='right'>Ganancia</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {report.bills.map((bill, index) => (
                            <TableRow key={index}>
                                <TableCell component='th' scope='row'>
                                    {bill.date}
                                </TableCell>
                                <TableCell align='right'>
                                    {bill.net_income}
                                </TableCell>
                                <TableCell align='right'>
                                    -{report.salary}
                                </TableCell>
                                <TableCell align='right'>
                                    -{bill.commission}
                                </TableCell>
                                <TableCell
                                    align='right'
                                    style={{
                                        color:
                                            bill.profit < 0
                                                ? '#f44336'
                                                : '#9e9e9e',
                                    }}
                                >
                                    {bill.profit}
                                </TableCell>
                            </TableRow>
                        ))}
                        <TableRow>
                            <TableCell component='th' scope='row'>
                                Saldo
                            </TableCell>
                            <TableCell align='right'>
                                {report.total.net_income}
                            </TableCell>
                            <TableCell align='right'>
                                -{report.salary * 2}
                            </TableCell>
                            <TableCell align='right'>
                                -{report.total.commission}
                            </TableCell>
                            <TableCell
                                align='right'
                                style={{
                                    color:
                                        report.total.profit < 0
                                            ? '#f44336'
                                            : '#009688',
                                }}
                            >
                                {report.total.profit}
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}
