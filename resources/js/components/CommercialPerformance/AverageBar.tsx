import React, { Component } from 'react';
import {
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ReferenceLine,
    ResponsiveContainer,
    BarChart,
} from 'recharts';
import { Report } from './models';

/**
 * Bar Chart Props
 */
interface AverageBarProps {
    report: Report[];
}

/**
 * Bar Chart State
 */
interface AverageBarState {
    firstMonth: string;
    names: string[];
    data: any[];
    averageSalary: number;
}

class AverageBarComponent extends Component<AverageBarProps, AverageBarState> {
    constructor(props: AverageBarProps) {
        super(props);
        const reports = props.report;

        const monthsChart: any[] = [];
        let salaries = 0;

        const names: string[] = [];

        // Transform data for Chart Component compatibility
        reports.forEach(report => {
            salaries += report.salary;
            const consultant = report.consultant.split(' ').join('');
            report.bills.forEach(bill => {
                if (bill.date) {
                    if (monthsChart.length < 1) {
                        monthsChart.push({
                            name: bill.date,
                            [consultant]: bill.net_income,
                        });
                    } else {
                        const foundMonth = monthsChart.findIndex(month => {
                            return bill.date === month.name;
                        });

                        if (foundMonth === -1) {
                            monthsChart.push({
                                name: bill.date,
                                [consultant]: bill.net_income,
                            });
                        } else {
                            monthsChart[foundMonth][consultant] =
                                bill.net_income;
                        }
                    }

                    const foundName = names.findIndex(name => {
                        return consultant === name;
                    });

                    foundName === -1 ? names.push(consultant) : false;
                }
            });
        });

        const averageSalary = salaries / reports.length;

        this.state = {
            data: monthsChart,
            names: names,
            firstMonth: reports[0].bills[0].date
                ? reports[0].bills[0].date
                : 'January de 2007',
            averageSalary: averageSalary,
        };
    }

    /**
     * Generate random color
     *
     * @returns Hexadecimal Color
     */
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    /**
     * Render Graphic/Bar Chart
     *
     * @returns JSX with chart
     */
    render() {
        const { data, names, firstMonth, averageSalary } = this.state;
        const colors: string[] = [];

        names.forEach((val, index) => {
            colors.push(this.getRandomColor());
        });

        return (
            <ResponsiveContainer aspect={16 / 9} width='100%' height='400px'>
                <BarChart
                    data={data}
                    margin={{
                        top: 20,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray='1 1' />
                    <XAxis dataKey='name' domain={[firstMonth, 'auto']} />
                    <YAxis domain={[-32000, 32000]} />
                    <Tooltip />
                    <Legend />
                    <ReferenceLine
                        y={averageSalary}
                        stroke='green'
                        label='Promedio'
                    />
                    <ReferenceLine y={0} x={0} stroke='#000' />
                    {names.map((id, index) => {
                        return (
                            <Bar
                                key={`${index}`}
                                dataKey={`${id}`}
                                fill={colors[index]}
                            />
                        );
                    })}
                </BarChart>
            </ResponsiveContainer>
        );
    }
}

export default AverageBarComponent;
