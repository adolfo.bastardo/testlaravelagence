import React, { Component } from 'react';
import { Tooltip, ResponsiveContainer, PieChart, Pie, Cell } from 'recharts';
import { Report } from './models';

/**
 * [[RADIAN]] const and [[renderCustomizedLabel]] function returns a custom Label for Pie Chart
 * info in recharts documentation
 */
const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index,
}: any) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
        <text
            x={x}
            y={y}
            fill='white'
            textAnchor={x > cx ? 'start' : 'end'}
            dominantBaseline='central'
        >
            {`${(percent * 100).toFixed(0)}%`}
        </text>
    );
};

/**
 * Pizza Chart Props
 */
interface AveragePizzaProps {
    report: Report[];
}

/**
 * Pizza Chart State
 */
interface AveragePizzaState {
    data: any[];
}

class AveragePizzaComponent extends Component<
    AveragePizzaProps,
    AveragePizzaState
> {
    constructor(props: AveragePizzaProps) {
        super(props);
        const reports = props.report;

        const monthsChart: any[] = [];

        // Transform data for Chart Component compatibility
        reports.forEach(report => {
            const consultant = report.consultant.split(' ').join('');
            report.bills.forEach(bill => {
                if (bill.date) {
                    if (monthsChart.length < 1) {
                        monthsChart.push({
                            name: consultant,
                            value: bill.net_income,
                        });
                    } else {
                        const foundMonth = monthsChart.findIndex(month => {
                            return consultant === month.name;
                        });

                        if (foundMonth === -1) {
                            monthsChart.push({
                                name: consultant,
                                value: bill.net_income,
                            });
                        } else {
                            monthsChart[foundMonth].value += bill.net_income;
                        }
                    }
                }
            });
        });

        this.state = {
            data: monthsChart,
        };
    }

    /**
     * Generate random color
     *
     * @returns Hexadecimal Color
     */
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    /**
     * Render Pizza Chart
     *
     * @returns JSX with chart
     */
    render() {
        const { data } = this.state;

        return (
            <ResponsiveContainer aspect={16 / 9} width='100%' height='100%'>
                <PieChart>
                    <Pie
                        dataKey='value'
                        startAngle={180}
                        endAngle={-180}
                        data={data}
                        fill='#8884d8'
                        label={renderCustomizedLabel}
                        labelLine={false}
                    >
                        {data.map((val, index) => (
                            <Cell key={index} fill={this.getRandomColor()} />
                        ))}
                    </Pie>
                    <Tooltip />
                </PieChart>
            </ResponsiveContainer>
        );
    }
}

export default AveragePizzaComponent;
