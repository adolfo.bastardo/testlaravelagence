import React, { Component, RefObject, ChangeEvent, MouseEvent } from 'react';
import ReactDOM from 'react-dom';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';

import InsertDriveFile from '@material-ui/icons/InsertDriveFile';
import InsertChartOutlined from '@material-ui/icons/InsertChartOutlined';
import PieChart from '@material-ui/icons/PieChart';

import { AxiosResponse } from 'axios';
import { Report, Bill } from './models';
import ReportComponent from './Report';
import AverageBarComponent from './AverageBar';
import AveragePizzaComponent from './AveragePizza';

const URL = 'http://localhost:8080';
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

/**
 * Check if a name is selected and return an appropriate font-weight value
 *
 * @param name  String for check if exists in consultant's list.
 * @param that  'this' object of Component to search.
 *
 * @returns Font weight number
 */
function getStyles(name: string, that: Component<any, any>) {
    return {
        fontWeight:
            that.state.consultantsSelected.indexOf(name) === -1 ? 400 : 500,
    };
}

/**
 * Return a formatted date string
 *
 * @param month  Month number.
 * @param year   Year number.
 *
 * @returns string date
 */
function getTime(month: number, year: number) {
    const MONTHS = [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
    ];

    const timestr = `${MONTHS[month - 1]} de ${year}`;

    return timestr;
}

/**
 * State Interface for CommercialPerformanceComponent
 */
interface CommercialPerformanceState {
    consultantsSelected: string | string[];
    labelWidth: number;
    consultants: any[];

    monthOne: number;
    yearOne: number;
    monthTwo: number;
    yearTwo: number;

    typeReport: string;
    typeAction: string;

    reports?: Report[];
}

class CommercialPerformanceComponent extends Component<
    any,
    CommercialPerformanceState
> {
    InputLabelRef: RefObject<InputLabel> = React.createRef();

    constructor(props: any) {
        super(props);

        this.state = {
            consultantsSelected: [],
            labelWidth: 0,
            consultants: [],
            monthOne: 1,
            yearOne: 2007,
            monthTwo: 2,
            yearTwo: 2007,
            typeReport: 'consultants',
            typeAction: '',
        };

        this.handleConsultantChange = this.handleConsultantChange.bind(this);
        this.getReports = this.getReports.bind(this);
        this.getChartBar = this.getChartBar.bind(this);
        this.getChartPizza = this.getChartPizza.bind(this);
    }

    /**
     * Get consultants list and set in State
     */
    componentDidMount() {
        const { consultants } = this.state;
        const inputlabel = ReactDOM.findDOMNode(
            this.InputLabelRef.current
        ) as HTMLLabelElement;
        if (inputlabel) {
            this.setState({
                labelWidth: inputlabel.offsetWidth,
            });
        }

        if (consultants.length === 0) {
            (window as any).axios
                .get(`${URL}/api/consultants`)
                .then((response: AxiosResponse) => {
                    this.setState({ consultants: response.data });
                });
        }
    }

    /**
     * Insert selected consultants to State
     *
     * @event ChangeEvent<HTMLSelectElement>
     * @param event  Event Object.
     *
     */
    handleConsultantChange(event: ChangeEvent<HTMLSelectElement>) {
        if (event !== null) {
            this.setState({
                consultantsSelected: event.target.value,
                reports: undefined,
                typeAction: '',
            });
        }
    }

    /**
     * Get report from BackEnd for all selected consultants and classify between actions
     *
     * @param action  String for action category.
     */
    getReportInfo(action: string) {
        const {
            typeReport,
            consultantsSelected,
            monthOne,
            yearOne,
            monthTwo,
            yearTwo,
        } = this.state;
        if (consultantsSelected.length > 0) {
            (window as any).axios
                .get(
                    `${URL}/api/report?type=${typeReport}&arrayPersonsFilter=${consultantsSelected}&monthStart=${monthOne}&yearStart=${yearOne}&monthEnd=${monthTwo}&yearEnd=${yearTwo}`
                )
                .then((response: AxiosResponse<any>) => {
                    let resp = response.data;
                    resp.forEach((val: any, index: number) => {
                        let consultant = this.state.consultants[
                            this.state.consultants.findIndex(consultant => {
                                return consultant.co_usuario == val.consultant;
                            })
                        ].no_usuario;
                        resp[index].consultant = consultant;

                        const bills: Bill[] = [];
                        Object.entries(val.bills as any[]).forEach(entry => {
                            const key = entry[0];
                            const value = entry[1];

                            bills.push({
                                date: key,
                                net_income: value['net_income'],
                                commission: value['commission'],
                                profit: value['profit'],
                            });
                        });

                        resp[index].bills = bills;
                    });
                    this.setState({ reports: resp, typeAction: action });
                });
        }
    }

    /**
     * Trigger report of type 'report' for general table report
     */
    getReports() {
        this.getReportInfo('report');
    }

    /**
     * Trigger report of type 'chartbar' for bar chart
     */
    getChartBar() {
        this.getReportInfo('chartbar');
    }

    /**
     * Trigger report of type 'chartpizza' for Pizza/Pie chart
     */
    getChartPizza() {
        this.getReportInfo('chartpizza');
    }

    /**
     * Render General Grid and Elements for Layout
     *
     * @returns JSX with princial layout
     */
    render() {
        return (
            <Grid
                container
                spacing={16}
                style={{
                    flexGrow: 1,
                    justifyContent: 'center',
                    margin: '6em 0 0',
                    overflow: 'hidden',
                    width: '100%',
                }}
            >
                <Grid item xs={12} md={10}>
                    <Grid container spacing={16} style={{ flexGrow: 1 }}>
                        <Grid item xs={12}>
                            <Grid
                                container
                                spacing={8}
                                style={{
                                    flexGrow: 1,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <Grid item xs={12} md={2}>
                                    <Typography variant='h5'>
                                        Periodo
                                    </Typography>
                                </Grid>
                                <Grid item xs={5} md={2}>
                                    <FormControl
                                        variant='outlined'
                                        style={{ margin: '8px', width: '100%' }}
                                    >
                                        <InputLabel
                                            htmlFor='outlined-age-native-simple'
                                            ref={this.InputLabelRef}
                                        >
                                            Mes
                                        </InputLabel>
                                        <Select
                                            native
                                            value={this.state.monthOne}
                                            onChange={(
                                                event: ChangeEvent<
                                                    HTMLSelectElement
                                                >
                                            ) => {
                                                this.setState({
                                                    monthOne: parseInt(
                                                        event.target.value,
                                                        10
                                                    ),
                                                });
                                            }}
                                            input={
                                                <OutlinedInput
                                                    name='age'
                                                    labelWidth={
                                                        this.state.labelWidth
                                                    }
                                                    id='outlined-age-native-simple'
                                                />
                                            }
                                        >
                                            <option value='' />
                                            <option value={1}>Enero</option>
                                            <option value={2}>Febrero</option>
                                            <option value={3}>Marzo</option>
                                            <option value={4}>Abril</option>
                                            <option value={5}>Mayo</option>
                                            <option value={6}>Junio</option>
                                            <option value={7}>Julio</option>
                                            <option value={8}>Agosto</option>
                                            <option value={9}>
                                                Septiembre
                                            </option>
                                            <option value={10}>Octubre</option>
                                            <option value={11}>
                                                Noviembre
                                            </option>
                                            <option value={12}>
                                                Diciembre
                                            </option>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={5} md={2}>
                                    <FormControl
                                        variant='outlined'
                                        style={{ margin: '8px', width: '100%' }}
                                    >
                                        <InputLabel
                                            htmlFor='outlined-age-native-simple'
                                            ref={this.InputLabelRef}
                                        >
                                            Año
                                        </InputLabel>
                                        <Select
                                            native
                                            value={this.state.yearOne}
                                            onChange={(
                                                event: ChangeEvent<
                                                    HTMLSelectElement
                                                >
                                            ) => {
                                                this.setState({
                                                    yearOne: parseInt(
                                                        event.target.value,
                                                        10
                                                    ),
                                                });
                                            }}
                                            input={
                                                <OutlinedInput
                                                    name='age'
                                                    labelWidth={
                                                        this.state.labelWidth
                                                    }
                                                    id='outlined-age-native-simple'
                                                />
                                            }
                                        >
                                            <option value='' />
                                            <option value={2003}>2003</option>
                                            <option value={2004}>2003</option>
                                            <option value={2005}>2005</option>
                                            <option value={2006}>2006</option>
                                            <option value={2007}>2007</option>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid
                                    item
                                    xs={2}
                                    style={{ textAlign: 'center' }}
                                >
                                    <Typography variant='body1'>a</Typography>
                                </Grid>
                                <Grid item xs={6} md={2}>
                                    <FormControl
                                        variant='outlined'
                                        style={{ margin: '8px', width: '100%' }}
                                    >
                                        <InputLabel
                                            htmlFor='outlined-age-native-simple'
                                            ref={this.InputLabelRef}
                                        >
                                            Mes
                                        </InputLabel>
                                        <Select
                                            native
                                            value={this.state.monthTwo}
                                            onChange={(
                                                event: ChangeEvent<
                                                    HTMLSelectElement
                                                >
                                            ) => {
                                                this.setState({
                                                    monthTwo: parseInt(
                                                        event.target.value,
                                                        10
                                                    ),
                                                });
                                            }}
                                            input={
                                                <OutlinedInput
                                                    name='age'
                                                    labelWidth={
                                                        this.state.labelWidth
                                                    }
                                                    id='outlined-age-native-simple'
                                                />
                                            }
                                        >
                                            <option value='' />
                                            <option value={1}>Enero</option>
                                            <option value={2}>Febrero</option>
                                            <option value={3}>Marzo</option>
                                            <option value={4}>Abril</option>
                                            <option value={5}>Mayo</option>
                                            <option value={6}>Junio</option>
                                            <option value={7}>Julio</option>
                                            <option value={8}>Agosto</option>
                                            <option value={9}>
                                                Septiembre
                                            </option>
                                            <option value={10}>Octubre</option>
                                            <option value={11}>
                                                Noviembre
                                            </option>
                                            <option value={12}>
                                                Diciembre
                                            </option>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6} md={2}>
                                    <FormControl
                                        variant='outlined'
                                        style={{ margin: '8px', width: '100%' }}
                                    >
                                        <InputLabel
                                            htmlFor='outlined-age-native-simple'
                                            ref={this.InputLabelRef}
                                        >
                                            Año
                                        </InputLabel>
                                        <Select
                                            native
                                            value={this.state.yearTwo}
                                            onChange={(
                                                event: ChangeEvent<
                                                    HTMLSelectElement
                                                >
                                            ) => {
                                                this.setState({
                                                    yearTwo: parseInt(
                                                        event.target.value,
                                                        10
                                                    ),
                                                });
                                            }}
                                            input={
                                                <OutlinedInput
                                                    name='age'
                                                    labelWidth={
                                                        this.state.labelWidth
                                                    }
                                                    id='outlined-age-native-simple'
                                                />
                                            }
                                        >
                                            <option value='' />
                                            <option value={2003}>2003</option>
                                            <option value={2004}>2003</option>
                                            <option value={2005}>2005</option>
                                            <option value={2006}>2006</option>
                                            <option value={2007}>2007</option>
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <Grid
                                container
                                spacing={8}
                                style={{
                                    flexGrow: 1,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <Grid item xs={12} md={2}>
                                    <Typography variant='h5'>
                                        Consultores
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} md={10}>
                                    <FormControl
                                        style={{ margin: '8px', width: '100%' }}
                                    >
                                        <InputLabel htmlFor='select-multiple-chip'>
                                            Seleccione a los consultores
                                        </InputLabel>
                                        <Select
                                            multiple
                                            value={
                                                this.state.consultantsSelected
                                            }
                                            onChange={
                                                this.handleConsultantChange
                                            }
                                            input={
                                                <Input id='select-multiple-chip' />
                                            }
                                            renderValue={selected => (
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        flexWrap: 'wrap',
                                                    }}
                                                >
                                                    {selected &&
                                                    Array.isArray(selected)
                                                        ? selected.map(
                                                              (
                                                                  value,
                                                                  index
                                                              ) => (
                                                                  <Chip
                                                                      key={
                                                                          index
                                                                      }
                                                                      label={
                                                                          this
                                                                              .state
                                                                              .consultants[
                                                                              this.state.consultants.findIndex(
                                                                                  consultant => {
                                                                                      return (
                                                                                          consultant.co_usuario ==
                                                                                          value
                                                                                      );
                                                                                  }
                                                                              )
                                                                          ]
                                                                              .no_usuario
                                                                      }
                                                                      style={{
                                                                          margin:
                                                                              16 /
                                                                              4,
                                                                      }}
                                                                  />
                                                              )
                                                          )
                                                        : null}
                                                </div>
                                            )}
                                            MenuProps={MenuProps}
                                        >
                                            {this.state.consultants.map(
                                                (data, index) => (
                                                    <MenuItem
                                                        key={index}
                                                        value={data.co_usuario}
                                                        style={getStyles(
                                                            data.co_usuario,
                                                            this
                                                        )}
                                                    >
                                                        {data.no_usuario}
                                                    </MenuItem>
                                                )
                                            )}
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12} md={2}>
                    <Grid container spacing={16}>
                        <Grid item xs={12}>
                            <Grid
                                container
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <Grid item xs={12}>
                                    <Button
                                        variant='contained'
                                        size='large'
                                        onClick={this.getReports}
                                        style={{
                                            verticalAlign: 'sub',
                                            width: '100%',
                                        }}
                                    >
                                        <InsertDriveFile
                                            style={{ margin: '0 .5rem' }}
                                        />
                                        Informe
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <Grid
                                container
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <Grid item xs={12}>
                                    <Button
                                        variant='contained'
                                        size='large'
                                        onClick={this.getChartBar}
                                        style={{
                                            verticalAlign: 'sub',
                                            width: '100%',
                                        }}
                                    >
                                        <InsertChartOutlined
                                            style={{ margin: '0 .5rem' }}
                                        />
                                        Gráfico
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <Grid
                                container
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                            >
                                <Grid item xs={12}>
                                    <Button
                                        variant='contained'
                                        size='large'
                                        onClick={this.getChartPizza}
                                        style={{
                                            verticalAlign: 'sub',
                                            width: '100%',
                                        }}
                                    >
                                        <PieChart
                                            style={{ margin: '0 .5rem' }}
                                        />
                                        Pizza
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                {this.state.reports && this.state.typeAction === 'report'
                    ? this.state.reports.map((report, index) => (
                          <Grid key={index} item xs={12}>
                              <ReportComponent report={report} />
                          </Grid>
                      ))
                    : null}
                {this.state.reports && this.state.typeAction === 'chartbar' ? (
                    <Grid item xs={12} md={6} style={{ width: '100%' }}>
                        <div>
                            <AverageBarComponent report={this.state.reports} />
                        </div>
                    </Grid>
                ) : null}
                {this.state.reports &&
                this.state.typeAction === 'chartpizza' ? (
                    <Grid item xs={12} md={6} style={{ width: '100%' }}>
                        <div style={{ height: 400 }}>
                            <AveragePizzaComponent
                                report={this.state.reports}
                            />
                        </div>
                    </Grid>
                ) : null}
            </Grid>
        );
    }
}

export default CommercialPerformanceComponent;
