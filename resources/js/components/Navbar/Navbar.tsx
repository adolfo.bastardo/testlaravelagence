import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';
import AssignmentTurnedIn from '@material-ui/icons/AssignmentTurnedIn';
import AccountBalanceWallet from '@material-ui/icons/AccountBalanceWallet';
import SupervisorAccount from '@material-ui/icons/SupervisorAccount';
import AccountBalance from '@material-ui/icons/AccountBalance';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ExitToApp from '@material-ui/icons/ExitToApp';
import { Hidden } from '@material-ui/core';

/**
 * Props Interface for [[NavbarComponent]]
 */
interface NavbarProps {}

/**
 * State Interface for [[NavbarComponent]]
 */
interface NavbarState {
    anchorEl: HTMLElement | null;
}
class NavbarComponent extends Component<NavbarProps, NavbarState> {
    constructor(props: NavbarProps) {
        super(props);

        this.state = {
            anchorEl: null,
        };

        this.handleProfileMenuOpen = this.handleProfileMenuOpen.bind(this);
        this.handleMenuClose = this.handleMenuClose.bind(this);
    }

    /**
     * Add Element for submenú
     *
     * @event React.MouseEvent
     * @param event  Event Object.
     *
     */
    handleProfileMenuOpen(event: React.MouseEvent) {
        this.setState({ anchorEl: event.currentTarget as HTMLElement });
    }

    /**
     * Close all submenus
     */
    handleMenuClose() {
        this.setState({ anchorEl: null });
    }

    /**
     * Render JSX for Navbar
     */
    render() {
        const { anchorEl } = this.state;
        const isMenuOpen = Boolean(anchorEl);

        // Render Sub-Component for submenu
        const renderMenu = (
            <Menu
                anchorEl={anchorEl}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={isMenuOpen}
                onClose={this.handleMenuClose}
            >
                <MenuItem onClick={this.handleMenuClose}>
                    Perfomance Comercial
                </MenuItem>
            </Menu>
        );

        return (
            <React.Fragment>
                <AppBar position='fixed' color='default'>
                    <Toolbar>
                        <Hidden smDown>
                            <div
                                style={{
                                    flexGrow: 1,
                                }}
                            >
                                <img src='/img/logo.gif' alt='' />
                            </div>
                        </Hidden>
                        <Button style={{ minWidth: '43px' }}>
                            <HomeIcon />
                            <Hidden smDown>Agence</Hidden>
                        </Button>
                        <Button style={{ minWidth: '43px' }}>
                            <AssignmentTurnedIn />
                            <Hidden smDown>Proyectos</Hidden>
                        </Button>
                        <Button style={{ minWidth: '43px' }}>
                            <AccountBalanceWallet />
                            <Hidden smDown>Administrativo</Hidden>
                        </Button>
                        <Button
                            aria-owns={
                                isMenuOpen ? 'material-appbar' : undefined
                            }
                            aria-haspopup='true'
                            color='primary'
                            onClick={this.handleProfileMenuOpen}
                            style={{ minWidth: '43px' }}
                        >
                            <SupervisorAccount />
                            <Hidden smDown>Comercial</Hidden>
                        </Button>
                        <Button style={{ minWidth: '43px' }}>
                            <AccountBalance />
                            <Hidden smDown>Financiero</Hidden>
                        </Button>
                        <Button style={{ minWidth: '43px' }}>
                            <AccountCircle />
                            <Hidden smDown>Usuario</Hidden>
                        </Button>
                        <Button color='secondary' style={{ minWidth: '43px' }}>
                            <ExitToApp />
                            <Hidden smDown>Salir</Hidden>
                        </Button>
                    </Toolbar>
                </AppBar>
                {renderMenu}
            </React.Fragment>
        );
    }
}

export default NavbarComponent;
